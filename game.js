import utils from "./utils.js"
import Pokemon from "./pokemon.js"
import {pokemons} from "./pokemoni.js"

class Game {
    constructor() {
        this.player1 = new Pokemon ({
            ...this.generatePlayer(), 
            selectors: 'player1',
        });
        this.player2 = new Pokemon ({
            ...this.generatePlayer(), 
            selectors: 'player2',
        })
        this.over = false; 
    }

    start() {
        console.log('Game started!');
        this.player1.renderPokemon();
        this.player2.renderPokemon();
    }

    restart() {
        console.log("game restart");
        this.player1 = new Pokemon ({
            ...this.generatePlayer(), 
            selectors: 'player1',
        });
        this.player2 = new Pokemon ({
            ...this.generatePlayer(), 
            selectors: 'player2',
        })
        this.over = false;
        this.start();
    }

    generatePlayer() {
        return pokemons[utils.random(pokemons.length) - 1];
    }
}

export default Game; 